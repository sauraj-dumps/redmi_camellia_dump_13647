## gsi_gms_arm64-user 12 SPB5.210812.003 7673742 release-keys
- Manufacturer: xiaomi
- Platform: mt6833
- Codename: camellia
- Brand: Redmi
- Flavor: gsi_gms_arm64-user
- Release Version: 12
- Id: SPB5.210812.003
- Incremental: 7673742
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/camellia/camellia:11/RP1A.200720.011/V12.0.8.0.RKSCNXM:user/release-keys
- OTA version: 
- Branch: gsi_gms_arm64-user-12-SPB5.210812.003-7673742-release-keys
- Repo: redmi_camellia_dump_13647


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
