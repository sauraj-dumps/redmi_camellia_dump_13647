#!/bin/bash

cat cust/app/customized/partner-com.zhiliaoapp.musically_42/partner-com.zhiliaoapp.musically_42.apk.* 2>/dev/null >> cust/app/customized/partner-com.zhiliaoapp.musically_42/partner-com.zhiliaoapp.musically_42.apk
rm -f cust/app/customized/partner-com.zhiliaoapp.musically_42/partner-com.zhiliaoapp.musically_42.apk.* 2>/dev/null
cat cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk.* 2>/dev/null >> cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk
rm -f cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk.* 2>/dev/null
cat cust/app/customized/partner-com.ss.android.ugc.trill_101/partner-com.ss.android.ugc.trill_101.apk.* 2>/dev/null >> cust/app/customized/partner-com.ss.android.ugc.trill_101/partner-com.ss.android.ugc.trill_101.apk
rm -f cust/app/customized/partner-com.ss.android.ugc.trill_101/partner-com.ss.android.ugc.trill_101.apk.* 2>/dev/null
cat cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk.* 2>/dev/null >> cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk
rm -f cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk.* 2>/dev/null
cat cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk.* 2>/dev/null >> cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk
rm -f cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk.* 2>/dev/null
cat system/system/product/priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk.* 2>/dev/null >> system/system/product/priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk
rm -f system/system/product/priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk.* 2>/dev/null
cat system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/system/product/priv-app/Velvet/Velvet.apk
rm -f system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/system/product/app/YouTube/YouTube.apk.* 2>/dev/null >> system/system/product/app/YouTube/YouTube.apk
rm -f system/system/product/app/YouTube/YouTube.apk.* 2>/dev/null
cat system/system/product/app/Maps/Maps.apk.* 2>/dev/null >> system/system/product/app/Maps/Maps.apk
rm -f system/system/product/app/Maps/Maps.apk.* 2>/dev/null
cat system/system/product/app/PrebuiltGmail/PrebuiltGmail.apk.* 2>/dev/null >> system/system/product/app/PrebuiltGmail/PrebuiltGmail.apk
rm -f system/system/product/app/PrebuiltGmail/PrebuiltGmail.apk.* 2>/dev/null
cat system/system/product/app/Photos/Photos.apk.* 2>/dev/null >> system/system/product/app/Photos/Photos.apk
rm -f system/system/product/app/Photos/Photos.apk.* 2>/dev/null
cat system/system/system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system/system/system_ext/apex/com.android.vndk.v30.apex
rm -f system/system/system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system/system/system_ext/apex/com.android.vndk.v29.apex.* 2>/dev/null >> system/system/system_ext/apex/com.android.vndk.v29.apex
rm -f system/system/system_ext/apex/com.android.vndk.v29.apex.* 2>/dev/null
cat system/system/system_ext/apex/com.android.vndk.v28.apex.* 2>/dev/null >> system/system/system_ext/apex/com.android.vndk.v28.apex
rm -f system/system/system_ext/apex/com.android.vndk.v28.apex.* 2>/dev/null
cat vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> vendor/data-app/SmartHome/SmartHome.apk
rm -f vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat vendor_boot/vendor_boot.img.* 2>/dev/null >> vendor_boot/vendor_boot.img
rm -f vendor_boot/vendor_boot.img.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
